package br.com.emmaos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.widget.EditText;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import br.com.emmaos.R;
import br.com.emmaos.dao.EmpresaDAO;
import br.com.emmaos.model.Empresa;

import static br.com.emmaos.activity.ConstantesActivities.CHAVE_EMPRESA;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_EDITA_EMPRESA;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_NOVA_EMPRESA;

public class FormularioEmpresaActivity extends AppCompatActivity {
    private EditText campoRazaoSocial;
    private EditText campoCnpj;
    private EditText campoTelefone;
    private final EmpresaDAO dao = new EmpresaDAO();
    private Empresa empresa;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_empresa);
        inicializacaoDosCampos();
        //configuraBotaoSalvar();
        carregaEmpresa();

    }

    private void carregaEmpresa() {
        Intent dados = getIntent();
        if (dados.hasExtra(CHAVE_EMPRESA)) {
            setTitle(TITULO_APPBAR_EDITA_EMPRESA);
            empresa = (Empresa) dados.getSerializableExtra(CHAVE_EMPRESA);
            preencheCamposEmpresa();
        } else {
            setTitle(TITULO_APPBAR_NOVA_EMPRESA);
            empresa = new Empresa();
        }
    }

    private void preencheCamposEmpresa() {
        campoRazaoSocial.setText(empresa.getRazaoSocial());
        campoCnpj.setText(empresa.getCnpj());
        campoTelefone.setText(empresa.getTelefone());
    }

    private void inicializacaoDosCampos() {
        campoRazaoSocial = findViewById(R.id.activity_formulario_empresa_razaoSocial);
        campoCnpj = findViewById(R.id.activity_formulario_empresa_cnpj);
        campoTelefone = findViewById(R.id.activity_formulario_empresa_telefone);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu_formulario_empresa, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if(itemId == R.id.activity_menu_botao_salvar_empresa){
            finalizaFormularioEmpresa();
        }
        return super.onOptionsItemSelected(item);
    }
    /*private void configuraBotaoSalvar() {
        Button botaoSalvar = findViewById(R.id.activity_formulario_empresa_botao_salvar);
        botaoSalvar.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finalizaFormularioEmpresa();
            }
        });
    }*/

    private void finalizaFormularioEmpresa() {
        preencheEmpresa();
        if (empresa.temIdValido()) {
            dao.edita(empresa);
        } else {

            dao.salva(empresa);
        }
        finish();
    }

    private void preencheEmpresa() {
        String razaoSocial = campoRazaoSocial.getText().toString();
        String cnpj = campoCnpj.getText().toString();
        String telefone = campoTelefone.getText().toString();
        empresa.setRazaoSocial(razaoSocial);
        empresa.setCnpj(cnpj);
        empresa.setTelefone(telefone);

    }
}
