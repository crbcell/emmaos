package br.com.emmaos.activity;

import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import br.com.emmaos.R;
import br.com.emmaos.dao.ClienteDAO;
import br.com.emmaos.model.Cliente;

import static br.com.emmaos.activity.ConstantesActivities.CHAVE_CLIENTE;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_LISTA_CLIENTES;

public class ListaClientesActivity extends AppCompatActivity {
    private final ListaClientesResponsabilitys
            listaClientesREsponsability = new ListaClientesResponsabilitys(this);
    private final ClienteDAO dao = new ClienteDAO();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_clientes);
        setTitle(TITULO_APPBAR_LISTA_CLIENTES);
        configuraFabNovoCliente();
        configuraLista();
    }

    @Override
    protected void onResume() {
        super.onResume();
        listaClientesREsponsability.atualizaClientes();
    }

    private void configuraLista() {
        ListView listaDeClientes = findViewById(R.id.activity_lista_clientes_listview);
        listaClientesREsponsability.configuraAdapter(listaDeClientes);
        registerForContextMenu(listaDeClientes);
    }



    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_menu_lista_cliente, menu);
    }

    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        identificaAcaoContextItem(itemId, menuInfo);
        return super.onContextItemSelected(item);
    }

    private void identificaAcaoContextItem(int itemId, AdapterView.AdapterContextMenuInfo menuInfo) {
        final Cliente clienteEscolhido = ListaClientesResponsabilitys.adapter.getItem(menuInfo.position);
        if (itemId == R.id.activity_menu_botao_editar_cliente) {
            abreFormularioModoEditaCliente(clienteEscolhido);
        } else if (itemId == R.id.activity_menu_botao_remover_cliente) {
            listaClientesREsponsability.ConfirmacaoDeExclusaoCliente(clienteEscolhido);
        }
    }


    private void abreFormularioModoEditaCliente(Cliente clienteEscolhido) {
        Intent vaiParaFormularioClienteActivity = new Intent(ListaClientesActivity.this,
                FormularioClienteActivity.class);
        vaiParaFormularioClienteActivity.putExtra(CHAVE_CLIENTE, clienteEscolhido);
        startActivity(vaiParaFormularioClienteActivity);
    }

    private void configuraFabNovoCliente() {
        FloatingActionButton botaoNovoCliente = findViewById(R.id.activity_lista_clientes_fab_novo_cliente);
        botaoNovoCliente.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abreFormularioNovoClienteActivity();
            }
        });
    }

    private void abreFormularioNovoClienteActivity() {
        startActivity(new Intent(this, FormularioClienteActivity.class));
    }
}
