package br.com.emmaos.activity;

import android.content.Context;
import android.widget.ListView;

import br.com.emmaos.adapter.ListaEmpresasAdapter;
import br.com.emmaos.dao.EmpresaDAO;
import br.com.emmaos.model.Empresa;

public class ListaEmpresaResponsability {

    private final EmpresaDAO dao = new EmpresaDAO();
    public ListaEmpresasAdapter adapter;
    private Context context;

    public ListaEmpresaResponsability(Context context) {
        this.context = context;
        this.adapter = new ListaEmpresasAdapter(context);
    }

    public void atualizaEmpresas() {
        adapter.atualiza(dao.todasEmpresas());
    }

    void configuraAdapter(ListView listaDeEmpresas) {
        adapter = new ListaEmpresasAdapter(context);
        listaDeEmpresas.setAdapter(adapter);
    }
    public void remove(Empresa empresa) {
        adapter.remove(empresa);

    }
}
