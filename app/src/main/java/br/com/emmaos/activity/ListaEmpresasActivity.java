package br.com.emmaos.activity;

import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.view.ContextMenu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AlertDialog;
import androidx.appcompat.app.AppCompatActivity;

import com.google.android.material.floatingactionbutton.FloatingActionButton;

import br.com.emmaos.R;
import br.com.emmaos.dao.EmpresaDAO;
import br.com.emmaos.model.Empresa;

import static br.com.emmaos.activity.ConstantesActivities.CHAVE_EMPRESA;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_LISTA_EMPRESAS;

public class ListaEmpresasActivity extends AppCompatActivity {

    private ListaEmpresaResponsability listaEmpresaResponsability = new ListaEmpresaResponsability(this);
    private final EmpresaDAO dao = new EmpresaDAO();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_lista_empresas);
        setTitle(TITULO_APPBAR_LISTA_EMPRESAS);
        configuracaoFabNovaEmpresa();
        configuraLista();

    }


    @Override
    protected void onResume() {
        super.onResume();
        listaEmpresaResponsability.atualizaEmpresas();
    }


    private void configuraLista() {
        ListView listaDeEmpresas = findViewById(R.id.activity_lista_empresas_listview);
        listaEmpresaResponsability.configuraAdapter(listaDeEmpresas);
        registerForContextMenu(listaDeEmpresas);
    }


    @Override
    public boolean onContextItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        AdapterView.AdapterContextMenuInfo menuInfo = (AdapterView.AdapterContextMenuInfo) item.getMenuInfo();
        identificaAcaoContextItem(itemId, menuInfo);
        return super.onContextItemSelected(item);
    }

    private void identificaAcaoContextItem(int itemId, AdapterView.AdapterContextMenuInfo menuInfo) {
        final Empresa empresaEscolhida = listaEmpresaResponsability.adapter.getItem(menuInfo.position);
        if (itemId == R.id.activity_menu_botao_remover_cliente) {
            new AlertDialog.Builder(ListaEmpresasActivity.this)
                    .setTitle("Remover Empresa")
                    .setMessage("Tem certeza?")
                    .setPositiveButton("Sim", null)
                    .setNegativeButton("Não", new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            listaEmpresaResponsability.remove(empresaEscolhida);
                        }
                    }).show();
            //listaEmpresaResponsability.remove(empresaEscolhida);
        } else if (itemId == R.id.activity_menu_botao_editar_cliente) {
            abreFormularioModoEditaEmpresa(empresaEscolhida);
        }
    }

    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        getMenuInflater().inflate(R.menu.activity_menu_lista_cliente, menu);
    }

    /*private void configuraListenerDeCliquePorItem(ListView listaDeClientes) {
        listaDeClientes.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                Empresa empresaEscolhida = (Empresa) parent.getItemAtPosition(position);
                abreFormularioModoEditaEmpresa(empresaEscolhida);
            }
        });
    }

    private void configuraListenerDeCliqueLongoPorItem(ListView listaDeClientes) {
        listaDeClientes.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                Empresa empresaEscolhida = (Empresa) parent.getItemAtPosition(position);
                return remove(empresaEscolhida);
            }
        });
    }*/



    private void abreFormularioModoEditaEmpresa(Empresa empresaEscolhida) {
        Intent vaiParaFormularioEmpresaActivity = new Intent(ListaEmpresasActivity.this, FormularioEmpresaActivity.class);
        vaiParaFormularioEmpresaActivity.putExtra(CHAVE_EMPRESA, empresaEscolhida);
        startActivity(vaiParaFormularioEmpresaActivity);
    }

    private void configuracaoFabNovaEmpresa() {
        FloatingActionButton botaoNovaEmpresa = findViewById(R.id.activity_lista_empresas_fab_nova_empresa);
        botaoNovaEmpresa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                abreFormularioNovaEmpresa();
            }
        });
    }

    private void abreFormularioNovaEmpresa() {
        startActivity(new Intent(ListaEmpresasActivity.this, FormularioEmpresaActivity.class));
    }
}
