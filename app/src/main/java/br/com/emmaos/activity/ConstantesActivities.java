package br.com.emmaos.activity;

public interface ConstantesActivities {
    String TITULO_APPBAR_NOVA_EMPRESA = "Nova Empresa";
    String TITULO_APPBAR_NOVA_CLIENTE = "Novo CLiente";
    String TITULO_APPBAR_EDITA_EMPRESA = "Editar Empresa";
    String TITULO_APPBAR_EDITA_CLIENTE = "Editar Cliente";
    String TITULO_APPBAR_LISTA_EMPRESAS = "Lista de Empresas";
    String TITULO_APPBAR_LISTA_CLIENTES  = "Lista de Clientes";
    String CHAVE_EMPRESA = "empresa";
    String CHAVE_CLIENTE = "cliente";
}
