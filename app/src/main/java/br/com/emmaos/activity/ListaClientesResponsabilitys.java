package br.com.emmaos.activity;

import android.app.AlertDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.widget.ListView;

import br.com.emmaos.adapter.ListaClientesAdapter;
import br.com.emmaos.dao.ClienteDAO;
import br.com.emmaos.model.Cliente;

public class ListaClientesResponsabilitys {

    public static ListaClientesAdapter adapter;
    private final ClienteDAO dao = new ClienteDAO();
    private Context context;

    public ListaClientesResponsabilitys(Context context) {
        this.context = context;
        this.adapter = new ListaClientesAdapter(context);
    }

    public void atualizaClientes() {
        adapter.atualiza(dao.todosClientes());
    }

    public void configuraAdapter(ListView listaDeClientes) {
        adapter = new ListaClientesAdapter(context);
        listaDeClientes.setAdapter(adapter);
    }
    public void ConfirmacaoDeExclusaoCliente(final Cliente clienteEscolhido) {
        new AlertDialog.Builder(context)
                .setTitle("Removendo CLiente")
                .setMessage("Você confirma a exclusao do cliente?")
                .setNegativeButton("Não", null)
                .setPositiveButton("Sim", new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        remove(clienteEscolhido);
                    }
                })
                .show();
    }
    private void remove(Cliente cliente) {
        adapter.remove(cliente);

    }
}
