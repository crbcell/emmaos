package br.com.emmaos.activity;

import androidx.annotation.NonNull;
import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

import java.io.Serializable;

import br.com.emmaos.R;
import br.com.emmaos.dao.ClienteDAO;
import br.com.emmaos.model.Cliente;

import static br.com.emmaos.activity.ConstantesActivities.CHAVE_CLIENTE;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_EDITA_CLIENTE;
import static br.com.emmaos.activity.ConstantesActivities.TITULO_APPBAR_NOVA_CLIENTE;

public class FormularioClienteActivity extends AppCompatActivity {

    private EditText campoPrimeiroNome;
    private EditText campoUltimoSobrenome;
    private EditText campoTelefone;
    private final ClienteDAO dao = new ClienteDAO();
    private Cliente cliente;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_formulario_cliente);
        inticializacaoDosCampos();
        carregaClientes();

    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.activity_menu_formulario_cliente, menu);
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(@NonNull MenuItem item) {
        int itemId = item.getItemId();
        if (itemId == R.id.activity_menu_botao_salvar_cliente) {
            finalizaFormularioCliente();
        }
        return super.onOptionsItemSelected(item);
    }

    private void carregaClientes() {
        Intent dados = getIntent();
        if (dados.hasExtra(CHAVE_CLIENTE)) {
            cliente = (Cliente) dados.getSerializableExtra(CHAVE_CLIENTE);
            setTitle(TITULO_APPBAR_EDITA_CLIENTE);
            preencheCamposClientes();
        } else {
            setTitle(TITULO_APPBAR_NOVA_CLIENTE);
            cliente = new Cliente();
        }
    }

    private void preencheCamposClientes() {
        campoPrimeiroNome.setText(cliente.getPrimeiroNome());
        campoUltimoSobrenome.setText(cliente.getUltimoNome());
        campoTelefone.setText(cliente.getTelefone());
    }

    private void inticializacaoDosCampos() {
        campoPrimeiroNome = findViewById(R.id.activity_formulario_cliente_primeiroNome);
        campoUltimoSobrenome = findViewById(R.id.activity_formulario_cliente_ultimoSobrenome);
        campoTelefone = findViewById(R.id.activity_formulario_cliente_telefone);
    }

    private void finalizaFormularioCliente() {
        preencheCliente();
        if (cliente.temIdValido()) {
            dao.edita(cliente);
        } else {
            dao.salva(cliente);
        }
        finish();
    }

    private void preencheCliente() {
        String primeiroNome = campoPrimeiroNome.getText().toString();
        String ultimoSobrenome = campoUltimoSobrenome.getText().toString();
        String telefone = campoTelefone.getText().toString();
        cliente.setPrimeiroNome(primeiroNome);
        cliente.setUltimoNome(ultimoSobrenome);
        cliente.setTelefone(telefone);
    }
}
