package br.com.emmaos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.emmaos.R;
import br.com.emmaos.model.Cliente;

public class ListaClientesAdapter extends BaseAdapter {
    private final List<Cliente> clientes = new ArrayList<>();
    private Context context;

    public ListaClientesAdapter(Context context) {
        this.context = context;
    }

    @Override
    public int getCount() {
        return clientes.size();
    }

    @Override
    public Cliente getItem(int position) {
        return clientes.get(position);
    }

    @Override
    public long getItemId(int position) {
        return clientes.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewCriada = LayoutInflater.from(context)
                .inflate(R.layout.item_lista_cliente, parent, false);
        Cliente clienteEscolhido = clientes.get(position);
        preencheViewCriada(viewCriada, clienteEscolhido);
        return viewCriada;
    }

    private void preencheViewCriada(View viewCriada, Cliente clienteEscolhido) {
        TextView nome = viewCriada.findViewById(R.id.id_item_cliente_nome);
        TextView telefone = viewCriada.findViewById(R.id.id_item_cliente_telefone);
        nome.setText(clienteEscolhido.getPrimeiroNome());
        telefone.setText(clienteEscolhido.getTelefone());
    }

    public void atualiza(List<Cliente> clientes) {
        this.clientes.clear();
        this.clientes.addAll(clientes);
        notifyDataSetChanged();
    }

    public void remove(Cliente cliente) {
        clientes.remove(cliente);
        notifyDataSetChanged();
    }
}
