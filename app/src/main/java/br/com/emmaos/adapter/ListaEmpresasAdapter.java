package br.com.emmaos.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import br.com.emmaos.R;
import br.com.emmaos.model.Empresa;

public class ListaEmpresasAdapter extends BaseAdapter {
    private final List<Empresa> empresas = new ArrayList<>();
    private Context context;

    public ListaEmpresasAdapter(Context context){
        this.context = context;
    }

    @Override
    public int getCount() {
        return empresas.size();
    }

    @Override
    public Empresa getItem(int position) {
        return empresas.get(position);
    }

    @Override
    public long getItemId(int position) {
        return empresas.get(position).getId();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View viewCriada = LayoutInflater.from(context)
                .inflate(R.layout.item_lista_empresa, parent, false);
        Empresa empresaEscolhida = empresas.get(position);
        preencheViewCriada(viewCriada, empresaEscolhida);
        return viewCriada;
    }

    private void preencheViewCriada(View viewCriada, Empresa empresaEscolhida) {
        TextView nome = viewCriada.findViewById(R.id.id_item_empresa_nome);
        TextView telefone = viewCriada.findViewById(R.id.id_item_empresa_telefone);
        nome.setText(empresaEscolhida.getRazaoSocial());
        telefone.setText(empresaEscolhida.getTelefone());
    }

    public void atualiza(List<Empresa> empresas) {
        this.empresas.clear();
        this.empresas.addAll(empresas);
        notifyDataSetChanged();
    }

    public void remove(Empresa empresa) {
        empresas.remove(empresa);
        notifyDataSetChanged();
    }
}
