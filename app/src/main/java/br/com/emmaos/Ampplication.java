package br.com.emmaos;

import android.app.Application;

import br.com.emmaos.dao.ClienteDAO;
import br.com.emmaos.dao.EmpresaDAO;
import br.com.emmaos.model.Cliente;
import br.com.emmaos.model.Empresa;

public class Ampplication extends Application {
    private final ClienteDAO dao = new ClienteDAO();
    private final EmpresaDAO dao2 = new EmpresaDAO();
    @Override
    public void onCreate() {
        super.onCreate();
        dao.salva(new Cliente("Cristian", "Celestino", "123456"));
        dao.salva(new Cliente("Manuela", "Damazio", "123456"));

        dao2.salva(new Empresa("Mercado Guanabara", "12.123.123/0001-01", "123456"));
        dao2.salva(new Empresa("Mercado da Esquina", "12.123.123/0001-01", "123456"));
    }
}
