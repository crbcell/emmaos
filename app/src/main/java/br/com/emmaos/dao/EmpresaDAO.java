package br.com.emmaos.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.emmaos.model.Empresa;

public class EmpresaDAO {
    private final static List<Empresa> empresas = new ArrayList<>();
    private static int contadorDeId = 1;

    public void salva(Empresa empresa) {
        empresa.setId(contadorDeId);
        empresas.add(empresa);
        contadorDeId++;
    }

    public List<Empresa> todasEmpresas() {
        return new ArrayList<>(empresas);
    }

    public void edita(Empresa empresa) {
        Empresa empresaEncontrada = buscaEmpresaPorId(empresa);
        if (empresaEncontrada != null) {
            int posicaoDaEmpresa = empresas.indexOf(empresaEncontrada);
            empresas.set(posicaoDaEmpresa, empresa);
        }
    }

    private Empresa buscaEmpresaPorId(Empresa empresa) {
        for (Empresa e : empresas) {
            if (e.getId() == empresa.getId()) {
                return e;
            }
        }
        return null;
    }

    public void remove(Empresa empresa) {
        Empresa empresaEncontrada = buscaEmpresaPorId(empresa);
        if(empresaEncontrada != null){
            empresas.remove(empresaEncontrada);
        }
    }

}
