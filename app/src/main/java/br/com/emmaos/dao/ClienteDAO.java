package br.com.emmaos.dao;

import java.util.ArrayList;
import java.util.List;

import br.com.emmaos.model.Cliente;

public class ClienteDAO {
    private final static List<Cliente> clientes = new ArrayList<>();
    private static int contadorDeIds = 1;

    public void salva(Cliente cliente) {
        cliente.setId(contadorDeIds);
        clientes.add(cliente);
        contadorDeIds++;
    }

    public List<Cliente> todosClientes() {
        return new ArrayList<>(clientes);
    }

    public void edita(Cliente cliente) {
        Cliente clienteEncontrado = buscaClientePorId(cliente);
        if (clienteEncontrado != null){
            int posicaoDoCliente = clientes.indexOf(clienteEncontrado);
            clientes.set(posicaoDoCliente, cliente);
        }
    }

    private Cliente buscaClientePorId(Cliente cliente) {
        for (Cliente c : clientes) {
            if (c.getId() == cliente.getId()) {
                return c;
            }
        }
        return null;
    }

    public void remove(Cliente cliente) {
        Cliente clienteDevolvido = buscaClientePorId(cliente);
        if(clienteDevolvido != null){
            clientes.remove(clienteDevolvido);
        }
    }
}
