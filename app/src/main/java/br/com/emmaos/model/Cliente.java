package br.com.emmaos.model;

import androidx.annotation.NonNull;

import java.io.Serializable;

public class Cliente implements Serializable {
    private int id = 0;
    private String primeiroNome;
    private String ultimoNome;
    private String telefone;

    public Cliente(String primeiroNome, String ultimoNome, String telefone) {
        this.primeiroNome = primeiroNome;
        this.ultimoNome = ultimoNome;
        this.telefone = telefone;
    }

    public Cliente() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getPrimeiroNome() {
        return primeiroNome;
    }

    public void setPrimeiroNome(String primeiroNome) {
        this.primeiroNome = primeiroNome;
    }

    public String getUltimoNome() {
        return ultimoNome;
    }

    public void setUltimoNome(String ultimoNome) {
        this.ultimoNome = ultimoNome;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @NonNull
    @Override
    public String toString() {
        return primeiroNome;
    }

    public boolean temIdValido() {
        return id > 0;
    }
}
