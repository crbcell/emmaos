package br.com.emmaos.model;

import java.io.Serializable;

public class Empresa implements Serializable {

    private int id = 0;
    private String razaoSocial;
    private String cnpj;
    private String telefone;

    public Empresa(String razaoSocial, String cnpj, String telefone) {
        this.razaoSocial = razaoSocial;
        this.cnpj = cnpj;
        this.telefone = telefone;
    }

    public Empresa() {

    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getRazaoSocial() {
        return razaoSocial;
    }

    public void setRazaoSocial(String razaoSocial) {
        this.razaoSocial = razaoSocial;
    }

    public String getCnpj() {
        return cnpj;
    }

    public void setCnpj(String cnpj) {
        this.cnpj = cnpj;
    }

    public String getTelefone() {
        return telefone;
    }

    public void setTelefone(String telefone) {
        this.telefone = telefone;
    }

    @Override
    public String toString() {
        return razaoSocial;
    }

    public boolean temIdValido() {
        return id > 0;
    }
}
